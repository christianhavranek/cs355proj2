var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT *' +
        ' FROM apartment_complexes;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(complex_id, callback) {
    var query = 'SELECT *' +
        ' FROM apartment_complexes ' +
        'WHERE complex_id = ?';
    var queryData = [complex_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.getOffice = function(complex_id, callback){
    var query = 'SELECT * FROM office_information ' +
        'WHERE complex_id = ?';
    var queryData = [complex_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
}

exports.getAmenities = function(complex_id, callback){
    var query = 'SELECT * FROM  amenities ' +
        'WHERE complex_id = ?';
    queryData = [complex_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
}


exports.getApartments = function(complex_id, callback){
    var query = 'SELECT * FROM  apartments ' +
        'WHERE complex_id = ?';
    var queryData = [complex_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
}


exports.getAvailApartments = function(complex_id, callback){
    var query = 'SELECT a.* FROM ' +
        '(SELECT * FROM apartments WHERE available = \'yes\') a ' +
        'WHERE complex_id = ?';
    var queryData = [complex_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
}

exports.getAllApartments = function(callback){
    var query = 'CALL apartment_rent()';
    console.log(query);

    connection.query(query, function(err, result) {
        callback(err, result);
    });
}


exports.getReviews = function(complex_id, callback){
    var query = 'SELECT * FROM complex_reviews ' +
        'WHERE complex_id = ?';
    var queryData = [complex_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
}

exports.insert = function(params, callback) {

    var query = 'INSERT INTO complex_reviews (complex_id, description, rating) VALUES (?,?,?)';

    var queryData = ([params.complex_id, params.description, params.rating]);

    connection.query(query, queryData, function (err, result) {
        callback(err, result);
    });
};