var express = require('express');
var router = express.Router();

router.get('/all', function(req,res) {
        res.render('about/aboutAll');
});

router.get('/contact', function(req, res) {
        res.render('about/contactInfo');
});

module.exports = router;