var express = require('express');
var router = express.Router();
var complexes_dal = require('../model/complexes_dal');

router.get('/all', function(req, res) {
    complexes_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('complexes/complexesViewAll', { 'result':result });
        }
    });

});

router.get('/', function(req, res){
    if(req.query.complex_id == null) {
        res.send('complex_id is null');
    }
    else {
        complexes_dal.getById(req.query.complex_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('complexes/complexesViewById', {'result': result});
            }
        });
    }
});

router.get('/office/', function(req, res){
    if(req.query.complex_id == null) {
        res.send('complex_id is null');
    }
    else {
        complexes_dal.getOffice(req.query.complex_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('complexes/officeViewById', {'result': result});
            }
        });
    }
});

router.get('/amenities/', function(req, res){
    if(req.query.complex_id == null) {
        res.send('complex_id is null');
    }
    else {
        complexes_dal.getAmenities(req.query.complex_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('complexes/amenitiesViewById', {'result': result});
            }
        });
    }
});

router.get('/apartmentsAll/', function(req, res){
    if(req.query.complex_id == null) {
        res.send('complex_id is null');
    }
    else {
        complexes_dal.getApartments(req.query.complex_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('complexes/apartmentsViewById', {'result': result});
            }
        });
    }
});


router.get('/apartmentsAvailable/', function(req, res){
    if(req.query.complex_id == null) {
        res.send('complex_id is null');
    }
    else {
        complexes_dal.getAvailApartments(req.query.complex_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('complexes/apartmentsAvailableById', {'result': result});
            }
        });
    }
});

router.get('/apartmentsTotal', function(req, res){
        complexes_dal.getAllApartments(function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('complexes/apartmentsAllAvailable', {'result': result});
            }
        });
});


router.get('/reviews/', function(req, res){
    if(req.query.complex_id == null) {
        res.send('complex_id is null');
    }
    else {
        complexes_dal.getReviews(req.query.complex_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('complexes/reviewsById', {'result': result});
            }
        });
    }
});

router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    //if (req.query.complex_id == null)
    //{
      //  res.send('company_id is null')
    //}
    complexes_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('complexes/reviewAdd', {'apartment_complexes': result});
        }
    });
});

router.get('/insert/', function(req, res){
    // simple validation

    if(req.query.rating == null) {
        res.send('A rating must be provided.');
    }
    else if(req.query.description == null) {
        res.send('A description must be provided');
    }
    else if(req.query.complex_id == null) {
        res.send('complex_id is null');
        }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        complexes_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/complexes/all');
            }
        });
    }
});


module.exports = router;